
/**
 * @author Chester Moses
 * @file RacingVenuePane.java
 * @date 2017-03-13
 * 
 * Changelog:
 * 2017-03-07
 *  -   Testing version of class created. (CM)
 *  -   Circles are laid out in a GridPane. (CM)
 *  -   Lines are drawn between Circles even if the window is resized. (CM)
 *  -   Vehicles are animated between Circles when update() is called. (CM)
 *  -   Circles will display numbers inside of them. (CM)
 *  -   Vehicles flip horizontally when traveling to the left. (CM)
 * 2017-03-08
 *  -   Testing vehicles are hidden until started; testing 15x15 grid. (CM)
 *  -   Added reset to clear the timeline and hide vehicles. (CM)
 * 2017-03-11
 *  -   Added basic pause and resume methods. (CM)
 * 2017-03-12
 *  -   Accepts vehicles from RaceSimulationGUI. (CM)
 *  -   Now creates the venue based on a file and pre-existing map. (CM)
 *  -   Displays a different color for a route based on its type. (CM)
 *  -   Will animate vehicles based on their position on their route. (CM)
 *  -   Applies a gradient to routes based on their slipe. (CM)
 * 2017-03-13
 *  -   Cleaned up comments. (CM)
 */

import java.io.BufferedReader;
import java.io.IOException;
import java.util.HashMap;
import java.util.LinkedList;
import java.util.List;

import javafx.animation.KeyFrame;
import javafx.animation.KeyValue;
import javafx.animation.Timeline;
import javafx.beans.binding.Bindings;
import javafx.geometry.Bounds;
import javafx.geometry.HPos;
import javafx.scene.layout.ColumnConstraints;
import javafx.scene.layout.GridPane;
import javafx.scene.layout.Pane;
import javafx.scene.layout.RowConstraints;
import javafx.scene.layout.StackPane;
import javafx.scene.paint.Color;
import javafx.scene.paint.CycleMethod;
import javafx.scene.paint.LinearGradient;
import javafx.scene.paint.Stop;
import javafx.scene.shape.Circle;
import javafx.scene.shape.Line;
import javafx.scene.text.Font;
import javafx.scene.text.FontWeight;
import javafx.scene.text.Text;
import javafx.scene.transform.Rotate;
import javafx.util.Duration;

/**
 * Graphically represents the Racing Venue as well as the Vehicles that are
 * racing on it.
 * 
 * @author Chester Moses
 */
public class RacingVenuePane extends StackPane {
    /**
     * The Timeline that animations are played on.
     */
    private Timeline animations;

    /**
     * The list of vehicles to animate.
     */
    private LinkedList<VehicleImageView> vehicles;

    /**
     * The pane animated cars are displayed on.
     */
    private Pane cars;

    /**
     * The nodes displays, keyed to their number.
     */
    private HashMap<Integer, Circle> intersections;

    /**
     * Creates a new RacingVenuePane, loading layout information about the
     * VenueMap from the provided BufferedReader, and then adding in all of the
     * edges located in the map to this panel.
     * 
     * @param venueFile
     *            points to information about laying out the map
     * @param map
     *            the map to render
     */
    public RacingVenuePane(BufferedReader venueFile, VenueMap map) {
        super();

        vehicles = new LinkedList<VehicleImageView>();
        cars = new Pane();

        Pane lines = new Pane();
        GridPane circles = new GridPane();

        // Add lines, then circles, then cars to the layout.

        getChildren().add(lines);
        getChildren().add(circles);
        getChildren().add(cars);

        // Create circles

        // Fill the Gridpane with rows and columns of equal width.
        ColumnConstraints columns = new ColumnConstraints();
        columns.setPercentWidth(10);
        for (int i = 0; i < 15; i++) {
            circles.getColumnConstraints().add(columns);
        }
        RowConstraints rows = new RowConstraints();
        rows.setPercentHeight(10);
        for (int i = 0; i < 15; i++) {
            circles.getRowConstraints().add(rows);
        }

        // For testing purposes.
        // circles.setGridLinesVisible(true);

        // Store the circles as they are created.
        intersections = new HashMap<Integer, Circle>();
        int[] checkpoints = map.getCheckpoints();
        String in;

        // Advance through the file to find layout information.
        try {
            do {
                in = venueFile.readLine().trim();
            } while (!in.equals("Layout"));
        } catch (IOException e) {
            e.printStackTrace();
            return;
        }

        for (int i = 0; i < map.getLocations(); i++) {
            StackPane circleStack = new StackPane();

            // Read from the file for information about the layout.
            try {
                in = venueFile.readLine().trim();
            } catch (IOException e) {
                e.printStackTrace();
                return;
            }
            String[] coords = in.split(" ");
            int column = Integer.parseInt(coords[0]);
            int row = Integer.parseInt(coords[1]);

            // Create circle, change color if it is a checkpoint.
            Circle c = new Circle(15);
            Color circleColor = Color.DODGERBLUE;
            for (int j : checkpoints) {
                if (i == j) {
                    circleColor = Color.SPRINGGREEN;
                    break;
                }
            }
            c.setFill(circleColor);
            c.setStroke(Color.BLACK);
            GridPane.setHalignment(c, HPos.CENTER);

            circleStack.getChildren().add(c);

            // Add numbers to each circle.
            Text number = new Text(Integer.toString(i));
            number.setFont(Font.font("System", FontWeight.BOLD, 16));
            circleStack.getChildren().add(number);

            circles.add(circleStack, row, column);

            intersections.put(i, c);
        }

        // Create lines between circles.

        // Keep track of added routes.
        LinkedList<VenueRoute> addedRoutes = new LinkedList<VenueRoute>();

        for (int i = 0; i < map.getLocations(); i++) {
            List<VenueRoute> routes = map.getRoutes(i);

            for (VenueRoute route : routes) {
                // Don't add routes we already put in.
                VenueRoute temp = new VenueRoute(route.getDestination(), route.getSource(), 0, 0);
                if (!addedRoutes.contains(temp)) {
                    final Circle start, end;
                    start = intersections.get(route.getSource());
                    end = intersections.get(route.getDestination());

                    // Need to work around JavaFX bugs - only draw left to
                    // right, top to bottom.
                    int sr = GridPane.getRowIndex(start.getParent());
                    int sc = GridPane.getColumnIndex(start.getParent());
                    int er = GridPane.getRowIndex(end.getParent());
                    int ec = GridPane.getColumnIndex(end.getParent());

                    if ((sr <= er && sc <= ec) || (sc < ec)) {

                        Line road = new Line();
                        road.setStrokeWidth(3);
                        road.setMouseTransparent(true);

                        // Change color based on route type.
                        Color routeColor;

                        switch (route.getRouteType()) {
                        case DIRT:
                            routeColor = Color.SADDLEBROWN;
                            break;
                        case HILL:
                            routeColor = Color.FORESTGREEN;
                            break;
                        case HAIRPIN:
                            routeColor = Color.FIREBRICK;
                            break;
                        case SMOOTH:
                        default:
                            routeColor = Color.BLACK;
                        }

                        // Flat
                        if (route.getSlope() == 0) {
                            road.setStroke(routeColor);
                        } else {
                            // Have a gradient on the road based on the
                            // intensity of the slope.
                            Color slopeColor = routeColor.interpolate(Color.WHITE, Math.abs(route.getSlope()) / 10);

                            // Paler color = higher up.
                            Stop[] stops;
                            LinearGradient lg;

                            // Dirty hacks
                            if (route.getSource() == 4 && route.getDestination() == 3) {
                                stops = new Stop[] { new Stop(0, routeColor), new Stop(1, slopeColor) };
                            } else if (route.getSource() == 10 && route.getDestination() == 21) {
                                stops = new Stop[] { new Stop(0, routeColor), new Stop(1, slopeColor) };
                            } else if (route.getSource() == 22 && route.getDestination() == 23) {
                                stops = new Stop[] { new Stop(0, routeColor), new Stop(1, slopeColor) };
                            } else if (route.getSlope() > 0) { // uphill
                                if (sr > er) {
                                    stops = new Stop[] { new Stop(0, slopeColor), new Stop(1, routeColor) };
                                } else {
                                    stops = new Stop[] { new Stop(0, routeColor), new Stop(1, slopeColor) };
                                }
                            } else { // downhill
                                stops = new Stop[] { new Stop(0, slopeColor), new Stop(1, routeColor) };
                            }

                            if (sr == er) {
                                lg = new LinearGradient(0, 0, 1, 0, true, CycleMethod.NO_CYCLE, stops);
                            } else if (sc == ec) {
                                lg = new LinearGradient(0, 0, 0, 1, true, CycleMethod.NO_CYCLE, stops);
                            } else if (sr < er) {
                                lg = new LinearGradient(0, 0, 0, 1, true, CycleMethod.NO_CYCLE, stops);
                            } else {
                                lg = new LinearGradient(0, 0, 1, 1, true, CycleMethod.NO_CYCLE, stops);
                            }
                            road.setStroke(lg);
                        }

                        // Have the lines place their endpoints beneath the
                        // centers of two circles.
                        road.startXProperty().bind(Bindings.createDoubleBinding(() -> {
                            Bounds b = start.getParent().getBoundsInParent();
                            return b.getMinX() + b.getWidth() / 2;
                        }, start.getParent().boundsInParentProperty()));
                        road.startYProperty().bind(Bindings.createDoubleBinding(() -> {
                            Bounds b = start.getParent().getBoundsInParent();
                            return b.getMinY() + b.getHeight() / 2;
                        }, start.getParent().boundsInParentProperty()));
                        road.endXProperty().bind(Bindings.createDoubleBinding(() -> {
                            Bounds b = end.getParent().getBoundsInParent();
                            return b.getMinX() + b.getWidth() / 2;
                        }, end.getParent().boundsInParentProperty()));
                        road.endYProperty().bind(Bindings.createDoubleBinding(() -> {
                            Bounds b = end.getParent().getBoundsInParent();
                            return b.getMinY() + b.getHeight() / 2;
                        }, end.getParent().boundsInParentProperty()));

                        lines.getChildren().add(road);

                        addedRoutes.add(route);
                    }
                }
            }
        }

        animations = new Timeline();
    }

    /**
     * Adds the provided vehicles to the pane, and moves them to their starting
     * points.
     * 
     * @param newVehicles
     *            the list of vehicles to add to the pane.
     */
    public void setVehicles(LinkedList<VehicleImageView> newVehicles) {
        vehicles.addAll(newVehicles);

        for (VehicleImageView vehicle : vehicles) {
            // Set up properties of vehicles.
            vehicle.setFitWidth(75);
            vehicle.setX(vehicle.getFitWidth() / -2);
            // Needed to flip horizontally.
            vehicle.setTranslateZ(vehicle.getBoundsInLocal().getWidth() / 2.0);
            vehicle.setRotationAxis(Rotate.Y_AXIS);

            // Get starting point of vehicle.
            Circle start = intersections.get(vehicle.getCurrentRoute().getSource());
            Bounds startB = start.getParent().getBoundsInParent();

            // Move vehicle to its starting point.
            vehicle.setTranslateX(startB.getMinX() + startB.getWidth() / 2);
            vehicle.setTranslateY(startB.getMinY() + startB.getHeight() / 2 - start.getRadius());
        }

        cars.getChildren().addAll(vehicles);
    }

    /**
     * Animates the vehicles to their new positions over a period of 1 second.
     * 
     * @author Chester Moses
     */
    public void update(long rate) {
        // Reset the timeline
        animations.stop();
        animations.getKeyFrames().clear();

        // Add animations.
        for (VehicleImageView vehicle : vehicles) {
            vehicle.setVisible(true);

            // Find starting and ending points.
            Circle startCircle, endCircle;
            startCircle = intersections.get(vehicle.getCurrentRoute().getSource());
            endCircle = intersections.get(vehicle.getCurrentRoute().getDestination());

            Bounds startB = startCircle.getParent().getBoundsInParent();
            Bounds endB = endCircle.getParent().getBoundsInParent();

            // Flip if vehicle is going to the left.
            if (startB.getMinX() > endB.getMinX()) {
                vehicle.setRotate(180);
            } else {
                vehicle.setRotate(0);
            }

            // Include circle radius for proper offset.
            double startX = startB.getMinX() + startB.getWidth() / 2;
            double startY = startB.getMinY() + startB.getHeight() / 2 - startCircle.getRadius();
            double endX = endB.getMinX() + endB.getWidth() / 2 - startX;
            double endY = endB.getMinY() + endB.getHeight() / 2 - startY - endCircle.getRadius();

            // Have vehicle move down the route at the specified update rate.
            animations.getKeyFrames()
                    .add(new KeyFrame(new Duration(rate),
                            new KeyValue(vehicle.translateXProperty(), startX + endX * vehicle.getRoutePercentage()),
                            new KeyValue(vehicle.translateYProperty(), startY + endY * vehicle.getRoutePercentage())));
        }

        // Start the timeline.
        animations.play();
    }

    /**
     * Stops playback and returns vehicles to their starting positions.
     * 
     * @author Chester Moses
     */
    public void reset() {
        // Reset the timeline.
        animations.stop();
        animations.getKeyFrames().clear();

        // Move vehicles back to the start.
        for (VehicleImageView vehicle : vehicles) {
            // Get starting point of vehicle.
            Circle start = intersections.get(vehicle.getCurrentRoute().getSource());
            Bounds startB = start.getParent().getBoundsInParent();

            // Move vehicle to its starting point.
            vehicle.setTranslateX(startB.getMinX() + startB.getWidth() / 2);
            vehicle.setTranslateY(startB.getMinY() + startB.getHeight() / 2 - start.getRadius());
        }
    }

    /**
     * Stops playback of the animation.
     * 
     * @author Chester Moses
     */
    public void pause() {
        animations.stop();
    }

    /**
     * Resumes playback of the animation.
     * 
     * @author Chester Moses
     */
    public void resume() {
        animations.play();
    }
}
