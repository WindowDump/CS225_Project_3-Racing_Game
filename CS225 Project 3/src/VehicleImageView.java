
/**
 * @author Chester Moses
 * @file VehicleImageView.java
 * @date 2017-03-12
 * 
 * Changelog:
 * 2017-03-11
 *  -   Basic version of the class. (CM)
 * 2017-03-12
 * 	-	Changed what properties are set in constructor. (CM)
 *  -   Added additional methods. (CM)
 */

import javafx.scene.image.Image;
import javafx.scene.image.ImageView;

/**
 * A class to provide an image representation of a vehicle while also allowing
 * access to information about the vehicle that the image represents.
 * 
 * @author Chester Moses
 */
public class VehicleImageView extends ImageView {
    /**
     * The vehicle this class represents.
     */
    private Vehicle vehicle;

    /**
     * Creates a new VehicleImageView, associating the provided image with the
     * vehicle.
     * 
     * @param img
     *            the image representing the vehicle
     * @param vehicle
     *            the vehicle to represent
     */
    public VehicleImageView(Image img, Vehicle vehicle) {
        super(img);

        this.vehicle = vehicle;

        // Set up properties.
        setPreserveRatio(true);
    }

    /**
     * Provides the route the vehicle is currently traveling on.
     * 
     * @return the route the vehicle is currently traveling on.
     */
    public VenueRoute getCurrentRoute() {
        return vehicle.getCurrentRoute();
    }

    /**
     * Provides the current speed of the vehicle.
     * 
     * @return the current speed of the vehicle.
     */
    public double getCurrentSpeed() {
        return vehicle.getCurrentSpeed();
    }

    /**
     * Returns this vehicle's current destination.
     * 
     * @return the number of this vehicle's current destination.
     */
    public int getDestination() {
        return vehicle.getCurrentDestination();
    }

    /**
     * Provides the name of the vehicle.
     * 
     * @return the name of the vehicle this object represents.
     */
    public String getName() {
        return vehicle.getName();
    }

    /**
     * Provides the time this vehicle took to complete the race.
     * 
     * @return the time the vehicle took to complete the race, -1 if it is still
     *         on the race.
     */
    public int getRaceTime() {
        return vehicle.getRaceTime();
    }

    /**
     * A percentage of far down the current route this vehicle currently is.
     * 
     * @return a number 0-1 of how far from the starting point to the ending
     *         point of the route this vehicle currently is.
     */
    public double getRoutePercentage() {
        return 1 - (vehicle.getRemainingDistance() / vehicle.getCurrentRoute().getLength());
    }
}
