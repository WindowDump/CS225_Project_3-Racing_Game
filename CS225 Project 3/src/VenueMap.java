
/**
 * @author Tyler Pagliazzo, Chester Moses
 * @file VenueMap.java
 * 
 * VenueMap is the Graph representation of the Map that the Vehicles will interact on. Each vehicle will go from 
 * one location to the next along an Edge which is represented by the VenueRoute class.
 */

import java.io.BufferedReader;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import java.util.LinkedList;

public class VenueMap {
    /**
     * The number of locations represented in this VenueMap.
     */
    private final int locations;

    /**
     * The locations that are valid points for vehicles to start at or be
     * required to go to.
     */
    private int[] checkpoints;

    /**
     * The routes between locations.
     */
    public List<List<VenueRoute>> routes;

    /**
     * Default constructor - creates an empty VenueMap with the specified
     * locations.
     * 
     * @param locations
     *            the number of locations in the map.
     * 
     * @author Tyler Pagliazzo, Chester Moses
     */
    public VenueMap(int locations) {
        if (locations < 0)
            throw new IllegalArgumentException("Number of locations should not be negative");

        this.locations = locations;
        checkpoints = new int[1];

        routes = new ArrayList<List<VenueRoute>>(locations);
        for (int i = 0; i < locations; i++) {
            routes.add(new LinkedList<VenueRoute>());
        }
    }

    /**
     * Adds the specified route to the map, along with its corresponding
     * inverse.
     * 
     * @param route
     *            the route to add to the map
     * 
     * @author Chester Moses
     */
    public void addRoute(VenueRoute route) {
        if (route == null) {
            return;
        }

        if (route.getSource() >= locations || route.getDestination() >= locations) {
            return;
        }

        // System.out.println("Adding in route from " + route.getSource() + " to
        // " + route.getDestination());
        routes.get(route.getSource()).add(route);

        // Add second route in reverse direction, with inverted slope.
        VenueRoute reverse = new VenueRoute(route.getDestination(), route.getSource(), route.getLength(),
                route.getSlope() * -1);
        reverse.setRouteType(route.getRouteType());

        // System.out.println("Adding in route from " + reverse.getSource() + "
        // to " + reverse.getDestination());
        routes.get(reverse.getSource()).add(reverse);

        return;
    }

    /**
     * Sets the checkpoints of this map to the specified values.
     * 
     * @param newCheckpoints
     *            the destinations on this map that are to be considered
     *            checkpoints.
     * 
     * @author Chester Moses
     */
    public void setCheckpoints(int[] newCheckpoints) {
        if (newCheckpoints == null) {
            return;
        }

        this.checkpoints = newCheckpoints;
    }

    /**
     * Returns the number of locations in the graph
     * 
     * @return the number of locations
     */
    public int getLocations() {
        return locations;
    }
    
    /**
     * 
     * @return an array of integers, checkpoints
     */
    public int[] getCheckpoints() {
        return checkpoints;
    }
    
    /**
     * The routes in a List of VenueRoutes
     * @param source
     * 				the source location
     * @return List of VenueRoutes at the source location (vertex)
     */
    public List<VenueRoute> getRoutes(int source) {
        return routes.get(source);
    }

    /**
     * creates an iterator
     * @param source
     * 				The source vertex
     * @return an Iterator of VenueRoute 
     */
    public Iterator<VenueRoute> routeIterator(int source) {
        return routes.get(source).iterator();
    }

    /**
     * returns a VenueMape created by a textFile
     * 
     * @param bR
     *            The reader containing the information to parse.
     * @author Chester Moses
     */
    public static VenueMap createMap(BufferedReader bR) {
        // Get number of locations.
        int locations;

        try {
            locations = Integer.parseInt(bR.readLine());
        } catch (IOException e1) {
            e1.printStackTrace();
            return null;
        }

        VenueMap newMap = new VenueMap(locations);

        // Get list of checkpoints.
        int[] checkpoints;

        try {
            String[] line = bR.readLine().split(" ");
            checkpoints = new int[line.length];
            for (int i = 0; i < line.length; i++) {
                checkpoints[i] = Integer.parseInt(line[i]);
            }
        } catch (IOException e1) {
            e1.printStackTrace();
            return null;
        }

        newMap.setCheckpoints(checkpoints);

        // Get number of routes.
        int routeCount;

        try {
            routeCount = Integer.parseInt(bR.readLine());
        } catch (IOException e1) {
            e1.printStackTrace();
            return null;
        }

        // Read in routes, add them to the map.
        for (int i = 0; i < routeCount; i++) {
            String[] line;
            try {
                line = bR.readLine().split(" ");
            } catch (IOException e) {
                e.printStackTrace();
                return null;
            }

            // <source> <destination> <length> <height>
            int source = Integer.parseInt(line[0]);
            int destination = Integer.parseInt(line[1]);
            double length = Double.parseDouble(line[2]);
            double slope = Double.parseDouble(line[3]);

            VenueRoute r = new VenueRoute(source, destination, length, slope);
            newMap.addRoute(r);
        }

        return newMap;
    }
}
