
/**
 * @author Chester Moses
 * @file RaceSimulationGUI.java
 * @date 2017-03-13
 * 
 * Changelog:
 * 2017-03-06
 *  -   Basic JavaFX application created. (CM)
 *  -   Loads images into ImageView and places them in the scene. (CM)
 *  -   Uses Timeline animation to move the images. (CM)
 * 2017-03-07
 *  -   Reorganized layout with a BorderPane. (CM)
 *  -   Placeholder elements for the final GUI in place. (CM)
 *  -   Now uses a RacingVenuePane to display the vehicles. (CM)
 *  -   Simple button created, calls update() on RacingVenuePane when pressed. (CM)
 * 2017-03-08
 *  -   Changed how button presses will be handled -
 *          this class does not need to implement EventHandler. (CM)
 *  -   Added resetRace() and associated button. (CM)
 * 2017-03-09
 *  -   Now parses the main directory for .png files. (CM)
 * 2017-03-10
 *  -   Added a ListView with a factory that loads all car images and displays them. (CM)
 *  -   Loads vehicle names and images when the program is launched. (CM)
 *  -   Able to remember selected vehicles from the selection screen. (CM)
 *  -   Basic player and opponent selecting screen. (CM)
 * 2017-03-11
 *  -   Prompts the user to choose appropriate number of player vehicles. (CM)
 *  -   Adds the vehicles chosen by the player to the status box. (CM)
 *  -   Randomly chooses vehicles and adds them to the status box. (CM)
 *  -   Changes buttons presented to the user as they make choices. (CM)
 *  -   Basic pausing of the race. (CM)
 *  -   Now replaces the root node for a scene rather than having multiple scenes. (CM)
 *  -   Loads info from text files about each car. (CM)
 * 2017-03-12
 * 	-	Change to the buttons displayed: a single button to start, pause, and resume. (CM)
 * 	-	Creates vehicles in RaceManager and VehicleImageViews. (CM)
 *  -   Now has a timer which updates the race and images. (CM)
 *  -   Format displayed speed. (CM)
 * 2017-03-13
 *  -   Changed background color of venue pane. (CM)
 *  -   Reset button will start the race again. (CM)
 *  -   Checks to see if the race has finished, announces winner. (CM)
 */

import java.io.BufferedReader;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.Reader;
import java.net.URI;
import java.nio.file.FileSystem;
import java.nio.file.FileSystems;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.text.DecimalFormat;
import java.util.Collections;
import java.util.HashMap;
import java.util.Iterator;
import java.util.LinkedList;
import java.util.Timer;
import java.util.TimerTask;
import java.util.stream.Stream;

import javafx.application.Application;
import javafx.application.Platform;
import javafx.beans.value.ChangeListener;
import javafx.beans.value.ObservableValue;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.geometry.HPos;
import javafx.geometry.Insets;
import javafx.geometry.Orientation;
import javafx.geometry.Pos;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.ChoiceBox;
import javafx.scene.control.ListCell;
import javafx.scene.control.ListView;
import javafx.scene.control.ScrollPane;
import javafx.scene.control.ScrollPane.ScrollBarPolicy;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.control.ToolBar;
import javafx.scene.layout.Background;
import javafx.scene.layout.BackgroundFill;
import javafx.scene.layout.BorderPane;
import javafx.scene.layout.CornerRadii;
import javafx.scene.layout.FlowPane;
import javafx.scene.layout.Priority;
import javafx.scene.layout.VBox;
import javafx.scene.paint.Color;
import javafx.scene.paint.CycleMethod;
import javafx.scene.paint.LinearGradient;
import javafx.scene.paint.Stop;
import javafx.scene.shape.Circle;
import javafx.scene.shape.Line;
import javafx.scene.text.Font;
import javafx.scene.text.Text;
import javafx.scene.text.TextAlignment;
import javafx.stage.Stage;
import javafx.util.Callback;

/**
 * The main class of the program, which initializes the GUI and all other
 * objects. The user will initially be shown the venue the race will take place
 * on, and then they will be able to choose the number of players, number of
 * random opponents, and the vehicles each player has.
 * 
 * @author Chester Moses
 */
public class RaceSimulationGUI extends Application {
    /**
     * The scen the application displays.
     */
    private Scene mainScene;

    /**
     * The pane containing the main controls and displays.
     */
    private BorderPane mainPane;
    /**
     * Pane which displays the venue and graphically represents the vehicles as
     * they move.
     */
    private RacingVenuePane venue;
    /**
     * The box where information on the vehicles is displayed.
     */
    private VBox statusBox;
    /**
     * The ToolBar which houses the controls for the main scene.
     */
    private ToolBar mainToolBar;
    /**
     * The button which allows the user to start, pause, and resume the race.
     */
    private Button activateButton;
    /**
     * The text which announces the winner of the race.
     */
    private Text finishedText;

    /**
     * The pane containing the controls allowing the user to choose the number
     * of players and opponents.
     */
    private FlowPane playerSelectPane;

    /**
     * The pane containing the controls allowing the user to choose the vehicles
     * for the players.
     */
    private BorderPane vehicleSelectPane;
    /**
     * The button the user presses to choose their vehicle.
     */
    private Button vehicleSelectButton;
    /**
     * The name of the vehicle currently selected by the user.
     */
    private String selectedVehicle;

    /**
     * The names of the vehicles that can be chosen from.
     */
    private LinkedList<String> vehicleNames;
    /**
     * The images associated with vehicles, allowing these images to be loaded
     * initially.
     */
    private HashMap<String, Image> vehicleImg;

    /**
     * The number of players in the game.
     */
    private int numPlayers;
    /**
     * Number of players that have not chosen a vehicle yet.
     */
    private int playersLeft;
    /**
     * The number of computer-chosen vehicles.
     */
    private int numOpponents;

    /**
     * The images representing the vehicles.
     */
    private LinkedList<VehicleImageView> vehicles;
    /**
     * The text associated with each vehicle.
     */
    private HashMap<VehicleImageView, Text> infoText;

    /**
     * The manager repsonsible for the race.
     */
    private RaceManager manager;

    /**
     * Timer that updates the simulation and GUI.
     */
    private Timer timer;
    /**
     * How often the simulation updates.
     */
    private static long updateRate = 500;

    /**
     * Main constructor - sets up the GUI and initializes all relevant
     * variables.
     */
    @Override
    public void start(Stage primaryStage) throws Exception {
        // Set up fields.

        selectedVehicle = null;
        numPlayers = 1;
        numOpponents = 3;
        playersLeft = 1;

        activateButton = null;
        finishedText = null;

        vehicles = new LinkedList<VehicleImageView>();
        infoText = new HashMap<VehicleImageView, Text>();

        timer = new Timer();

        // Create RaceManager.
        Reader venueFile;

        try {
            venueFile = new FileReader(new File("vehicleinfo/venue.txt"));
        } catch (FileNotFoundException e1) {
            venueFile = new InputStreamReader(getClass().getResourceAsStream("/vehicleinfo/venue.txt"));
        }

        BufferedReader venueReader = new BufferedReader(venueFile);
        manager = new RaceManager(venueReader);

        // Create RacingVenuePane
        venue = new RacingVenuePane(venueReader, manager.getMap());

        try {
            venueReader.close();
            venueFile.close();
        } catch (IOException e1) {
            e1.printStackTrace();
            return;
        }

        Background venuebg = new Background(
                new BackgroundFill(Color.rgb(200, 200, 200), new CornerRadii(0), new Insets(0)));
        venue.setBackground(venuebg);

        // Parse directory for vehicles and load images.
        vehicleNames = new LinkedList<String>();

        URI uri = RaceSimulationGUI.class.getResource("/vehicleinfo").toURI();
        Path runPath;
        if (uri.getScheme().equals("jar")) {
            FileSystem fileSystem = FileSystems.newFileSystem(uri, Collections.<String, Object>emptyMap());
            runPath = fileSystem.getPath("/vehicleinfo");
        } else {
            runPath = Paths.get(uri);
        }
        Stream<Path> walk = Files.walk(runPath, 1);
        for (Iterator<Path> it = walk.iterator(); it.hasNext();) {
            Path next = it.next();
            String name = next.toString();
            String[] files = name.split("/");
            if (files.length == 1) {
                files = name.split("\\\\");
            }
            String file = files[files.length - 1];
            if (file.substring(file.length() - 3).equalsIgnoreCase("png")) {
                if (!file.contains("UML")) {
                    String shortName = file.substring(0, file.length() - 4);
                    vehicleNames.add(shortName);
                }
            }
        }
        walk.close();

        Collections.sort(vehicleNames);
        vehicleImg = new HashMap<String, Image>();
        for (String vehicle : vehicleNames) {
            Image img;
            try {
                img = new Image(getClass().getResourceAsStream("/vehicleinfo/" + vehicle + ".png"));
            } catch (NullPointerException e1) {
                System.out.println("couldn't open " + vehicle + ".png");
                img = new Image("file:" + vehicle + ".png");
            }
            vehicleImg.put(vehicle, img);
        }

        // Set up main pane

        mainPane = new BorderPane();
        ScrollPane vehicleInfo = new ScrollPane();
        statusBox = new VBox();
        mainScene = new Scene(mainPane, 800, 600, Color.LIGHTGRAY);
        primaryStage.setScene(mainScene);

        // Set up info pane
        vehicleInfo.setHbarPolicy(ScrollBarPolicy.NEVER);
        vehicleInfo.setVbarPolicy(ScrollBarPolicy.ALWAYS);
        vehicleInfo.setPrefWidth(220);
        vehicleInfo.setContent(statusBox);
        vehicleInfo.setStyle("-fx-background-color:white");
        statusBox.setPrefWidth(210);

        // Create map legend.
        FlowPane legend = new FlowPane(10, 5);

        Text legendTitle = new Text("Map Legend");
        legendTitle.setTextAlignment(TextAlignment.CENTER);
        legendTitle.setWrappingWidth(210);
        legendTitle.setFont(new Font(16));
        legend.getChildren().add(legendTitle);

        Circle normal = new Circle(10);
        normal.setFill(Color.DODGERBLUE);
        normal.setStroke(Color.BLACK);
        legend.getChildren().add(normal);
        Text normalTitle = new Text("Intersection");
        legend.getChildren().add(normalTitle);

        Circle checkpoint = new Circle(10);
        checkpoint.setFill(Color.SPRINGGREEN);
        checkpoint.setStroke(Color.BLACK);
        legend.getChildren().add(checkpoint);
        Text checkpointTitle = new Text("Checkpoint");
        legend.getChildren().add(checkpointTitle);

        Line slope = new Line(0, 10, 100, 10);
        slope.setStrokeWidth(2);
        Stop[] stops = new Stop[] { new Stop(0, Color.BLACK), new Stop(1, Color.WHITE) };
        LinearGradient lg = new LinearGradient(0, 0, 1, 0, true, CycleMethod.NO_CYCLE, stops);
        slope.setStroke(lg);
        legend.getChildren().add(slope);

        Text slopeTitle = new Text("Sloped Road");
        legend.getChildren().add(slopeTitle);

        Line smooth = new Line(0, 10, 100, 10);
        smooth.setStrokeWidth(2);
        smooth.setStroke(Color.BLACK);
        legend.getChildren().add(smooth);

        Text smoothTitle = new Text("Smooth Road");
        legend.getChildren().add(smoothTitle);

        Line dirt = new Line(0, 10, 100, 10);
        dirt.setStrokeWidth(2);
        dirt.setStroke(Color.SADDLEBROWN);
        legend.getChildren().add(dirt);

        Text dirtTitle = new Text("Dirt Road");
        legend.getChildren().add(dirtTitle);

        Line hilly = new Line(0, 10, 100, 10);
        hilly.setStrokeWidth(2);
        hilly.setStroke(Color.FORESTGREEN);
        legend.getChildren().add(hilly);

        Text hillyTitle = new Text("Hilly Road");
        legend.getChildren().add(hillyTitle);

        Line hairpin = new Line(0, 10, 100, 10);
        hairpin.setStrokeWidth(2);
        hairpin.setStroke(Color.FIREBRICK);
        legend.getChildren().add(hairpin);

        Text hairpinTitle = new Text("Hairpin Road");
        legend.getChildren().add(hairpinTitle);

        statusBox.getChildren().add(legend);

        // Add buttons to toolbar
        Button choosePlayers = new Button("Choose Players");
        choosePlayers.setDefaultButton(true);
        choosePlayers.setOnAction(new EventHandler<ActionEvent>() {
            @Override
            public void handle(ActionEvent e) {
                changeToPlayerSelectScene();
            }
        });
        choosePlayers.setFont(new Font(16));

        mainToolBar = new ToolBar(choosePlayers);

        mainPane.setCenter(venue);
        mainPane.setLeft(vehicleInfo);
        mainPane.setBottom(mainToolBar);

        mainPane.prefHeightProperty().bind(mainScene.heightProperty());
        mainPane.prefWidthProperty().bind(mainScene.widthProperty());

        // Set up player selection scene.

        playerSelectPane = new FlowPane(Orientation.VERTICAL);
        playerSelectPane.setColumnHalignment(HPos.CENTER);
        playerSelectPane.setVgap(10);
        playerSelectPane.setAlignment(Pos.CENTER);

        Text playerLabel = new Text("Number of Players:");
        playerLabel.setFont(new Font(18));
        ChoiceBox<Integer> playerChoice = new ChoiceBox<Integer>(FXCollections.observableArrayList(1, 2, 3, 4));
        playerChoice.setValue(1);
        playerChoice.setStyle("-fx-font-size: 16;");

        // Update the fields based on the content of the ChoiceBox.
        // (+1 since it starts at 1)
        playerChoice.getSelectionModel().selectedIndexProperty().addListener(new ChangeListener<Number>() {
            @Override
            public void changed(ObservableValue<? extends Number> ov, Number oldValue, Number newValue) {
                numPlayers = newValue.intValue() + 1;
                playersLeft = newValue.intValue() + 1;
            }
        });

        Text opponentLabel = new Text("Number of Opponents:");
        opponentLabel.setFont(new Font(18));
        ChoiceBox<Integer> opponentChoice = new ChoiceBox<Integer>(
                FXCollections.observableArrayList(0, 1, 2, 3, 4, 5, 6, 7, 8));
        opponentChoice.setValue(3);
        opponentChoice.setStyle("-fx-font-size: 16;");

        // Update the fields based on the content of the ChoiceBox.
        opponentChoice.getSelectionModel().selectedIndexProperty().addListener(new ChangeListener<Number>() {
            @Override
            public void changed(ObservableValue<? extends Number> ov, Number oldValue, Number newValue) {
                numOpponents = newValue.intValue();
            }
        });

        Button playerConfirm = new Button("Choose Vehicles");
        playerConfirm.setDefaultButton(true);
        playerConfirm.setFont(new Font(16));
        playerConfirm.setOnAction(new EventHandler<ActionEvent>() {
            @Override
            public void handle(ActionEvent e) {
                changeToVehicleSelectScene();
            }
        });

        playerSelectPane.getChildren().addAll(playerLabel, playerChoice, opponentLabel, opponentChoice, playerConfirm);

        // Set up vehicle selection pane.

        vehicleSelectPane = new BorderPane();

        vehicleSelectButton = new Button();
        vehicleSelectButton.setDefaultButton(true);
        vehicleSelectButton.setOnAction(new EventHandler<ActionEvent>() {
            @Override
            public void handle(ActionEvent e) {
                chooseVehicle();
            }
        });
        vehicleSelectButton.setFont(new Font(16));
        BorderPane.setAlignment(vehicleSelectButton, Pos.BOTTOM_CENTER);

        vehicleSelectPane.setBottom(vehicleSelectButton);

        // Set up selection list.
        VBox carBox = new VBox();
        carBox.setAlignment(Pos.CENTER);

        ListView<String> carList = new ListView<String>();
        ObservableList<String> carItems = FXCollections.observableArrayList(vehicleNames);
        carList.setItems(carItems);
        carList.setCellFactory(new Callback<ListView<String>, ListCell<String>>() {
            @Override
            public ListCell<String> call(ListView<String> list) {
                return new VehicleSelectCell();
            }
        });

        carList.getSelectionModel().selectedItemProperty().addListener(new ChangeListener<String>() {
            public void changed(ObservableValue<? extends String> ov, String old_val, String new_val) {
                selectedVehicle = new_val;
            }
        });

        carBox.getChildren().addAll(carList);
        VBox.setVgrow(carList, Priority.ALWAYS);

        ScrollPane vehicleChoices = new ScrollPane();
        vehicleChoices.setHbarPolicy(ScrollBarPolicy.NEVER);
        vehicleChoices.setVbarPolicy(ScrollBarPolicy.ALWAYS);
        vehicleChoices.setContent(carBox);

        vehicleSelectPane.setCenter(carBox);

        // Finish setting up the application.

        primaryStage.setTitle("Racing Simulation");
        primaryStage.show();
    }

    /**
     * Changes to the scene which allows the user to select the number of
     * players.
     */
    private void changeToPlayerSelectScene() {
        mainScene.setRoot(playerSelectPane);
    }

    /**
     * Changes to the scene which allows the user to select the vehicles for the
     * players.
     */
    private void changeToVehicleSelectScene() {
        vehicleSelectButton.setText("Choose Vehicle for Player " + (numPlayers - playersLeft + 1));

        mainScene.setRoot(vehicleSelectPane);
    }

    /**
     * Adds the vehicle selected by the user to the program, if it is selected.
     * If all players have chosen a vehicle, then the specified number of random
     * vehicles are added afterwards, and the program returns to the main scene.
     */
    private void chooseVehicle() {
        // Don't do anything if no vehicle is chosen.
        if (selectedVehicle == null) {
            return;
        }

        // Create stat pane and add it to the
        BorderPane stat = createStatPane(selectedVehicle);

        // Change background color to indicate which player is which.
        if (numPlayers - playersLeft + 1 == 1) {
            stat.setStyle(stat.getStyle() + " -fx-background-color: #ffaaaa;");
        }
        if (numPlayers - playersLeft + 1 == 2) {
            stat.setStyle(stat.getStyle() + " -fx-background-color: #bbccff;");
        }
        if (numPlayers - playersLeft + 1 == 3) {
            stat.setStyle(stat.getStyle() + " -fx-background-color: #ffffaa;");
        }
        if (numPlayers - playersLeft + 1 == 4) {
            stat.setStyle(stat.getStyle() + " -fx-background-color: #aaffaa;");
        }

        statusBox.getChildren().add(stat);

        // Create the specified vehicle in the manager, and add appropriate
        // associations to remember it.
        Vehicle tempV = manager.addVehicle(selectedVehicle);
        VehicleImageView vimg = new VehicleImageView(vehicleImg.get(selectedVehicle), tempV);
        infoText.put(vimg, (Text) stat.getCenter());
        vehicles.add(vimg);

        // Go back to choose a new vehicle if there are more players.
        playersLeft--;
        if (playersLeft > 0) {
            vehicleSelectButton.setText("Choose Vehicle for Player " + (numPlayers - playersLeft + 1));
            selectedVehicle = null;
            return;
        }

        // Add random vehicles!
        LinkedList<String> randomVehicles = new LinkedList<String>(vehicleNames);

        // Remove vehicles the player already chose from this list.
        for (VehicleImageView v : vehicles) {
            randomVehicles.remove(v.getName());
        }

        // Choose random vehicles and create them.
        for (int i = 0; i < numOpponents; i++) {
            String vehicle = randomVehicles.remove((int) (Math.random() * randomVehicles.size()));

            stat = createStatPane(vehicle);
            statusBox.getChildren().add(stat);

            tempV = manager.addVehicle(vehicle);
            vimg = new VehicleImageView(vehicleImg.get(vehicle), tempV);
            infoText.put(vimg, (Text) stat.getCenter());
            vehicles.add(vimg);
        }

        // Prepare to start the race.

        manager.prepareRace();
        venue.setVehicles(vehicles);

        // Set up main toolbar.
        activateButton = new Button("Start");
        activateButton.setDefaultButton(true);
        activateButton.setOnAction(new EventHandler<ActionEvent>() {
            @Override
            public void handle(ActionEvent e) {
                startRace();
            }
        });
        activateButton.setFont(new Font(16));

        Button reset = new Button("Reset");
        reset.setOnAction(new EventHandler<ActionEvent>() {
            @Override
            public void handle(ActionEvent e) {
                resetRace();
            }
        });
        reset.setFont(new Font(16));

        finishedText = new Text("");
        finishedText.setFont(new Font(16));

        mainToolBar.getItems().setAll(activateButton, reset, finishedText);

        // Change to main scene.
        mainScene.setRoot(mainPane);
    }

    /**
     * Creates a BorderPane containing the image of the specified car and text
     * that describes its status.
     * 
     * @param vehicleName
     *            The name of the vehicle.
     * @return a BoderPane containing an image of the vehicle and Text.
     */
    private BorderPane createStatPane(String vehicleName) {
        if (vehicleName == null || vehicleImg.get(vehicleName) == null) {
            return null;
        }

        BorderPane stat = new BorderPane();

        ImageView img = new ImageView(vehicleImg.get(vehicleName));
        img.setFitWidth(75);
        img.setPreserveRatio(true);
        BorderPane.setMargin(img, new Insets(0, 5, 0, 0));
        BorderPane.setAlignment(img, Pos.CENTER_LEFT);

        Text info = new Text();
        info.setText(vehicleName + "\nReady to start.\n\n");
        BorderPane.setAlignment(info, Pos.TOP_LEFT);

        stat.setStyle("-fx-border-style: solid; " + "-fx-border-width: 2 0 0 0; " + "-fx-border-insets: 0 0 5 0; "
                + "-fx-padding: 5 0 0 0;");
        stat.setLeft(img);
        stat.setCenter(info);

        return stat;
    }

    /**
     * A class which will load the appropriate car image based on the provided
     * car name.
     * 
     * @author Chester Moses
     */
    private class VehicleSelectCell extends ListCell<String> {
        private ImageView imageView;

        @Override
        public void updateItem(String item, boolean empty) {
            super.updateItem(item, empty);
            if (empty) {
                setText(null);
                setGraphic(null);
            } else {
                Image carImg = vehicleImg.get(item);
                imageView = new ImageView(carImg);
                imageView.setPreserveRatio(true);

                // Load the description for this vehicle from the file
                // associated with it.

                Reader vehicleFile;

                try {
                    vehicleFile = new InputStreamReader(getClass().getResourceAsStream("vehicleinfo/" + item + ".txt"));
                } catch (NullPointerException e2) {
                    System.out.println(item + ".txt");
                    e2.printStackTrace();
                    return;
                }

                BufferedReader vehicleReader = new BufferedReader(vehicleFile);
                String description = "";

                try {
                    description = vehicleReader.readLine();
                } catch (IOException e) {
                    e.printStackTrace();
                }

                try {
                    vehicleReader.close();
                    vehicleFile.close();
                } catch (IOException e1) {
                    e1.printStackTrace();
                    return;
                }

                setText(item + "\n" + description);

                setFont(new Font(16));
                setGraphic(imageView);
                setGraphicTextGap(165 - carImg.getWidth());
                setStyle("-fx-border-style: solid; " + "-fx-border-width: 0 0 2 0; " + "-fx-border-insets: 5 0 0 0; "
                        + "-fx-padding: 0 0 5 0; ");
            }
        }
    }

    /**
     * Updates the text displaying information about the vehicles, checking to
     * see if the race has finished.
     */
    private void updateText() {
        DecimalFormat df = new DecimalFormat("#0.00");

        boolean finished = false;

        for (VehicleImageView v : vehicles) {
            Text target = infoText.get(v);
            String info = v.getName();
            info += "\nSpeed: " + df.format(v.getCurrentSpeed());
            VenueRoute route = v.getCurrentRoute();
            int destination = v.getDestination();
            if (destination == -1) {
                // Reached the end of its route
                info += "\nFinished at " + route.getDestination() + "!";
                info += "\nTotal race time: " + v.getRaceTime();
                finished = true;
            } else {
                info += "\nTraveling from " + route.getSource() + " to " + route.getDestination();
                info += "\nCurrent Destination: " + v.getDestination();
            }

            target.setText(info);
        }

        // Check to see if any vehicle finished
        if (finished) {
            vehicleWon();
        }
    }

    /**
     * Adds the fastest vehicle to the toolbar.
     */
    private void vehicleWon() {
        // Used for thread safety
        Platform.runLater(new Runnable() {
            @Override
            public void run() {
                // Find fastest time
                int fastTime = Integer.MAX_VALUE;
                String fastCar = "";
                for (VehicleImageView v : vehicles) {
                    if (v.getRaceTime() < 1) {
                    } else if (v.getRaceTime() < fastTime) {
                        fastCar = v.getName();
                        fastTime = v.getRaceTime();
                    }
                }

                finishedText.setText(fastCar + " wins!");
            }
        });

        // Check to see if the race is over.
        boolean allDone = true;
        for (VehicleImageView v : vehicles) {
            if (v.getRaceTime() < 1) {
                allDone = false;
            }
        }

        // Check to see if the race is done.
        if (allDone) {
            Platform.runLater(new Runnable() {
                @Override
                public void run() {
                    activateButton.setText("New Race");
                    activateButton.setOnAction(new EventHandler<ActionEvent>() {
                        @Override
                        public void handle(ActionEvent e) {
                            resetRace();
                        }
                    });
                }
            });

            timer.cancel();
        }
    }

    /**
     * Starts the race, starting the timer.
     */
    private void startRace() {
        venue.update(updateRate);
        timer = new Timer();
        timer.scheduleAtFixedRate(new TimerTask() {
            public void run() {
                manager.update();
                updateText();
                venue.update(updateRate);
            }
        }, 0, updateRate);

        activateButton.setText("Pause");
        activateButton.setOnAction(new EventHandler<ActionEvent>() {
            @Override
            public void handle(ActionEvent e) {
                pauseRace();
            }
        });
    }

    /**
     * Pauses the race, stopping the timer.
     */
    private void pauseRace() {
        venue.pause();
        timer.cancel();

        activateButton.setText("Resume");
        activateButton.setOnAction(new EventHandler<ActionEvent>() {
            @Override
            public void handle(ActionEvent e) {
                resumeRace();
            }
        });
    }

    /**
     * Resumes the race, starting the timer again.
     */
    private void resumeRace() {
        venue.resume();
        timer = new Timer();
        timer.scheduleAtFixedRate(new TimerTask() {
            public void run() {
                manager.update();
                updateText();
                venue.update(updateRate);
            }
        }, updateRate, updateRate);

        activateButton.setText("Pause");
        activateButton.setOnAction(new EventHandler<ActionEvent>() {
            @Override
            public void handle(ActionEvent e) {
                pauseRace();
            }
        });
    }

    /**
     * Resets the race, giving vehicles new routes, and moving them to their new
     * starting points.
     */
    private void resetRace() {
        timer.cancel();
        manager.prepareRace();
        venue.reset();
        updateText();

        activateButton.setText("Start");
        activateButton.setOnAction(new EventHandler<ActionEvent>() {
            @Override
            public void handle(ActionEvent e) {
                startRace();
            }
        });
        finishedText.setText("");
    }

    /**
     * Launches the application.
     * 
     * @param args
     *            Command line arguments (ignored)
     */
    public static void main(String[] args) {
        launch(args);
    }
}