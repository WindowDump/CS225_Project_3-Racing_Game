/**
 * @author Tyler Pagliazzo, Chester Moses
 * @file Vehicle.java
 * 
 * Vehicle class controls its own movements. Finds the shortest path on the map to its destinations using Dijkstra's algorithm.
 * Can change it's speed, and other attributes based on the values assigned to its elements. 
 **/

import java.util.HashSet;
import java.util.LinkedList;
import java.util.List;
import java.util.Queue;

public class Vehicle {

    /// ***** Field Variables *****///
	
	/**
	 * The name of the car
	 */
    private String name;
    
    /**
     * The power at which the car can use to brake
     */
    private double brakingPower;
    
    /**
     * The car's weight
     */
    private double weight;
    
    /**
     * The current speed of the car
     */
    private double currentSpeed;
    
    /**
     * The speed the car is capable of reaching
     */
    private double topSpeed;
    
    /**
     * The cars ability to turn
     */
    private double turnSpeed;
    
    /**
     * A queue of integer's representing the locations the car must visit
     */
    private Queue<Integer> destinations;// instantiate the stack
    
    /**
     * The VenueMap used by the car to drive on
     */
    private VenueMap map;
    
    /**
     * A LinkedList of Routes representing the car's current route it is on
     */
    private LinkedList<VenueRoute> currentRoute; // fix type to create Queue
    
    /**
     * The distance remaining on the car's route
     */
    private double distanceRemaining;
    
    /**
     * The time elapsed on the race
     */
    private int raceTime;
    
    /**
     * The grip of the wheels, used on turns
     */
    private double grip;
    
    /**
     * The weight transfer representing the car's suspension ability
     */
    private double weightTransfer;
    
    /**
     * The car's acceleration 
     */
    private double acceleration;

    /// ***** Constructor *****///

    public Vehicle(String name, double weight, double brakingPower, double grip, double turnSpeed,
            double weightTransfer, double topSpeed, double acceleration, VenueMap map) {
        this.name = name;
        this.weight = weight;
        this.map = map;
        this.brakingPower = brakingPower;
        this.grip = grip;
        this.turnSpeed = turnSpeed;
        this.weightTransfer = weightTransfer;
        this.topSpeed = topSpeed;
        this.acceleration = acceleration;
        destinations = new LinkedList<Integer>();
        currentRoute = new LinkedList<VenueRoute>();
        currentSpeed = 0;
        raceTime = 0;
    }

    /***** Getters *****/
    
    /**
     * 
     * @return the VenueMap of the game
     */
    public VenueMap getMap() {
        return map;
    }

    /**
     * 
     * @return destinations 
     * 					The current destination of the car in the Queue
     */
    public int getCurrentDestination() {
        if (destinations.size() == 0) {
            return -1;
        }
        return destinations.peek();
    }
    
    /**
     * 
     * @return currentRoute
     * 						The current route of the Vehicle
     */
    public VenueRoute getCurrentRoute() {
        return currentRoute.peek();
    }
    
    /**
     * 
     * @return currentSpeed
     * 						The current speed of the car
     */
    public double getCurrentSpeed() {
        return currentSpeed;
    }
    
    /**
     * 
     * @return the Name of the car
     */
    public String getName() {
        return name;
    }
    
    /**
     * 
     * @return the remaining distance on the route
     */
    public double getRemainingDistance() {
        return distanceRemaining;
    }

    /**
     * 
     * @return the time elapsed on during the race
     */
    public int getRaceTime() {
        if (destinations.size() == 0) {
            return raceTime;
        } else {
            return -1;
        }
    }
    
    /**
     * 
     * @param dest	
     * 				the destination Queue of Integers
     */
    public void setDestinations(Queue<Integer> dest) {
        destinations = dest;
        calculateRoute();
        distanceRemaining = currentRoute.peek().getLength();
        currentSpeed = 0;
        raceTime = 0;
    }

    /**
     * Has the vehicle change its position. It will calculate its route,
     * accelerate, and slow down when this is needed.
     * 
     * @return true if this vehicle has finished the race, false otherwise
     * @author Chester Moses
     */
    public boolean update() {
        if (distanceRemaining == 0) {
            // End of route
            if (currentRoute.size() <= 1) {
                // Destination Reached
                if (destinations.size() <= 1) {
                    // Last destination reached
                    // Race done
                    destinations.clear();
                    currentSpeed = 0;
                    distanceRemaining = 0;
                    return true;
                }
                // Find new route
                calculateRoute();
            } else {
                // Get next route
                currentRoute.pop();
            }

            // Start moving down next route
            VenueRoute route = currentRoute.peek();
            distanceRemaining = route.getLength() - currentSpeed;
        } else if (distanceRemaining - currentSpeed >= 0) {
            // Not at the end of the route
            VenueRoute route = currentRoute.peek();

            // Determine how fast we should be going at the end of the route.
            double endSpeed = turnSpeed;
            if (currentRoute.size() == 1 && destinations.size() <= 1) {
                endSpeed = 0;
            }

            // Determine how fast we will go if we accelerate
            double incSpeed = currentSpeed;
            if (incSpeed < topSpeed) {
                incSpeed += speedUp(route);
                if (incSpeed > topSpeed) {
                    incSpeed = topSpeed;
                }
            }

            // Check to see if we can stop if we speed up
            double brakingDist = brakingDistance(route, incSpeed, endSpeed);
            if (brakingDist < distanceRemaining) {
                // We can speed up
                currentSpeed = incSpeed;
            } else {
                // We can't speed up
                // See if we need to slow down now
                brakingDist = brakingDistance(route, currentSpeed, endSpeed);
                if (brakingDist <= distanceRemaining) {
                    // We need to slow down
                    currentSpeed -= slowDown(route);
                    if (currentSpeed < endSpeed) {
                        currentSpeed = endSpeed;
                    }
                }
            }

            // Move down route
            if (distanceRemaining - currentSpeed > 0) {
                distanceRemaining -= currentSpeed;
            } else {
                currentSpeed = endSpeed;
                distanceRemaining = 0;
            }
        } else {
            // Go to end of route and turn
            distanceRemaining = 0;
            if (currentRoute.size() == 1 && destinations.size() <= 1) {
                currentSpeed = brakingPower;
            } else {
                currentSpeed = turnSpeed;
            }
        }

        raceTime++;
        return false;
    }

    /******* Private Methods *******/

    /**
     * Using Dijkstra's Algorithm, finds the shortest route from the current
     * position to the next destination and loads it into currentRoute.
     */
    private void calculateRoute() {
        if (destinations.size() < 1) {
            return;
        }

        int start = destinations.poll();

        // System.out.println("Computing route from " + start + " to " +
        // destinations.peek());

        // Dijkstra's Algorithm
        // TOOD: Consider travel time, not just route length

        HashSet<Integer> computed = new HashSet<Integer>(map.getLocations());
        HashSet<Integer> uncomputed = new HashSet<Integer>(map.getLocations());
        double[] dist = new double[map.getLocations()];
        Integer[] pred = new Integer[map.getLocations()];

        // Set up dist and pred - maximum value and starting vertex, unless it's
        // the start
        for (int i = 0; i < map.getLocations(); i++) {
            if (i == start) {
                computed.add(i);
                dist[i] = 0;
                pred[i] = i;
            } else {
                uncomputed.add(i);
                dist[i] = Double.MAX_VALUE;
                pred[i] = start;
            }
        }

        // Add in the weights of the routes from the starting vertex
        for (VenueRoute r : map.getRoutes(start)) {
            dist[r.getDestination()] = r.getLength();
        }

        // Get the smallest weight in the dist array
        while (uncomputed.size() > 0) {
            int smallest = 1;
            double min = Double.MAX_VALUE;
            for (int i : uncomputed) {
                if (dist[i] < min) {
                    smallest = i;
                    min = dist[i];
                }
            }

            // remove the smallest and add it to computed
            uncomputed.remove(smallest);
            computed.add(smallest);

            // Check to see if we found a new shortest route
            for (VenueRoute route : map.getRoutes(smallest)) {
                int dest = route.getDestination();
                if (uncomputed.contains(dest) && dist[smallest] + route.getLength() < dist[dest]) {
                    dist[dest] = dist[smallest] + route.getLength();
                    pred[dest] = smallest;
                }
            }
        }

        // Load the computed route into currentRoute
        int target = destinations.peek();
        currentRoute.clear();
        do {
            // Get route from pred[target] to target.
            List<VenueRoute> routes = map.getRoutes(pred[target]);
            VenueRoute nextRoute = null;
            for (VenueRoute route : routes) {
                if (route.getDestination() == target) {
                    // New target is pred[target]
                    nextRoute = route;
                    target = pred[target];
                    break;
                }
            }
            if (nextRoute == null) {
                System.out.print(name + " found a null route, destinations: ");
                for (int d : destinations) {
                    System.out.print(d + " ");
                }
                System.out.println();
                System.out.println(target + " to " + pred[target]);
            }
            currentRoute.push(nextRoute);
            // Repeat until we get to the starting point.
        } while (target != start);
    }

    /**
     * Determines how much distance the vehicle needs to slow from the starting
     * speed to the ending speed on the specified route.
     * 
     * @param route
     *            the route to consider
     * @param startingSpeed
     *            the starting speed on the route
     * @param endingSpeed
     *            the desired ending speed
     * @return the distance the vehicle will need to be able to slow down to the
     *         ending speed
     * @author Chester Moses
     */
    private double brakingDistance(VenueRoute route, double startingSpeed, double endingSpeed) {
        double distance = 0;
        double braking = slowDown(route);
        startingSpeed -= braking;
        while (startingSpeed > endingSpeed) {
            distance += startingSpeed;
            startingSpeed -= braking;
        }

        return distance;
    }

    /**
     * Determines how much the vehicle can speed up by on the specified route.
     * 
     * @param route
     *            the route to consider
     * @return the amount the vehicle can speed up by
     * @author Chester Moses
     */
    private double speedUp(VenueRoute route) {
        double accel = acceleration;

        // Consider route type
        switch (route.getRouteType()) {
        case DIRT:
            accel *= (0.5 + grip / 20.0);
            break;
        case HILL:
            accel *= (0.75 + weightTransfer / 40.0);
            break;
        case HAIRPIN:
            accel *= (0.5 + weightTransfer / 20.0);
            break;
        case SMOOTH:
        default:
            break;
        }

        // Consider slope & weight
        // harder to accelerate uphill
        // easier to accelerate downhill
        accel -= route.getSlope() * weight / 30.0;

        return accel;
    }

    /**
     * Determines how much the vehicle can slow down by on the specified route.
     * 
     * @param route
     *            the route to consider
     * @return the amount the vehicle can slow down by
     * @author Chester Moses
     */
    private double slowDown(VenueRoute route) {
        double braking = brakingPower;

        // Consider route type
        switch (route.getRouteType()) {
        case DIRT:
            braking *= (0.5 + grip / 20.0);
            break;
        case HILL:
            braking *= (0.75 + weightTransfer / 40.0);
            break;
        case SMOOTH:
        case HAIRPIN:
        default:
            break;
        }

        // Consider slope & weight
        // less braking power downhill
        // more braking power uphill
        braking += route.getSlope() * weight / 30.0;

        return braking;
    }
}
