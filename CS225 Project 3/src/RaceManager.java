
/**
 * @author Chester Moses, Tyler Pagliazzo
 * @file RaceManager.java
 */

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.Reader;
import java.util.ArrayList;
import java.util.LinkedList;
import java.util.List;
import java.util.Queue;

public class RaceManager {
    private LinkedList<Vehicle> vehicles;
    private VenueMap map;
    ActionListener moveCar = new ActionListener() {
        public void actionPerformed(ActionEvent evt) {

        }
    };

    //// *** Constructor ***////

    public RaceManager(BufferedReader in) {
        // Pass input file to VenueMap to create the map.
        map = VenueMap.createMap(in);
        vehicles = new LinkedList<Vehicle>();
    }

    /**
     * @return the map of the Venue
     */

    public VenueMap getMap() {
        return map;
    }

    /**
     * Assigns each vehicle a random, unique route with no duplicates.
     * 
     * @author Chester Moses
     */
    public void prepareRace() {
        List<Queue<Integer>> randomRoutes = new ArrayList<Queue<Integer>>(vehicles.size());

        for (int i = 0; i < vehicles.size(); i++) {
            randomRoutes.add(new LinkedList<Integer>());
        }

        int[] checkpoints = map.getCheckpoints();

        // Choose 4 random sets of exclusive numbers,
        // to ensure every vehicle has a unique route.
        for (int i = 0; i < 4; i++) {
            boolean duplicate;
            int[] layer;

            // Find a list of numbers with no duplicates.
            do {
                ArrayList<Integer> unassignedCheckpoints = new ArrayList<Integer>(checkpoints.length);
                for (int j = 0; j < checkpoints.length; j++) {
                    unassignedCheckpoints.add(checkpoints[j]);
                }

                duplicate = false;
                layer = new int[vehicles.size()];

                for (int j = 0; j < layer.length; j++) {
                    int checkpoint = unassignedCheckpoints.remove((int) (Math.random() * unassignedCheckpoints.size()));

                    // Make a new list if there are duplicates when the numbers
                    // would be assigned.
                    Queue<Integer> q = randomRoutes.get(j);
                    if (randomRoutes.get(j).size() > 0 && q.contains(checkpoint)) {
                        duplicate = true;
                        break;
                    }

                    layer[j] = checkpoint;
                }
            } while (duplicate);

            for (int j = 0; j < vehicles.size(); j++) {
                randomRoutes.get(j).offer(layer[j]);
            }
        }

        for (Vehicle v : vehicles) {
            v.setDestinations(randomRoutes.remove(randomRoutes.size() - 1));
        }
    }

    /**
     * calls the vehicle update method
     */
    public void update() {
        for (Vehicle v : vehicles) {
            v.update();
        }
    }

    /**
     * Creates a new vehicle by loading in a text file with the name provided.
     * This vehicle is then added to the RaceManager and returned.
     * 
     * @author Chester Moses
     * 
     * @param vehicleName
     *            The name of the vehicle to create.
     * @return The created vehicle, or null if it could not be created.
     */
    public Vehicle addVehicle(String vehicleName) {
        // Load the description for this vehicle from the file
        // associated with it.

        Reader vehicleFile;

        try {
            vehicleFile = new InputStreamReader(getClass().getResourceAsStream("vehicleinfo/" + vehicleName + ".txt"));
        } catch (NullPointerException e2) {
            System.out.println(vehicleName + ".txt");
            e2.printStackTrace();
            return null;
        }

        BufferedReader vehicleReader = new BufferedReader(vehicleFile);

        // Parse file for properties.
        double weight, topSpeed, accel, braking, turnSpeed, grip, weightTransfer;

        try {
            // Read past description line.
            String line = vehicleReader.readLine();

            // Property is the first "word" on each line.
            line = vehicleReader.readLine().split(" ")[0];
            weight = Double.parseDouble(line);

            line = vehicleReader.readLine().split(" ")[0];
            topSpeed = Double.parseDouble(line);

            line = vehicleReader.readLine().split(" ")[0];
            accel = Double.parseDouble(line);

            line = vehicleReader.readLine().split(" ")[0];
            braking = Double.parseDouble(line);

            line = vehicleReader.readLine().split(" ")[0];
            turnSpeed = Double.parseDouble(line);

            line = vehicleReader.readLine().split(" ")[0];
            grip = Double.parseDouble(line);

            line = vehicleReader.readLine().split(" ")[0];
            weightTransfer = Double.parseDouble(line);
        } catch (IOException e) {
            e.printStackTrace();
            return null;
        }

        try {
            vehicleReader.close();
            vehicleFile.close();
        } catch (IOException e1) {
            e1.printStackTrace();
            return null;
        }

        Vehicle v = new Vehicle(vehicleName, weight, braking, grip, turnSpeed, weightTransfer, topSpeed, accel, map);
        vehicles.add(v);

        return v;
    }
}
