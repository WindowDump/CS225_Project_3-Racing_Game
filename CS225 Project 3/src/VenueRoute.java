/**
 * @author Tyler Pagliazzo
 * @file VenueRoute.java
 * 
 *       VenueRoute is a simple class that holds the data of the edges of the
 *       map acting as the "Roads" of the venue The Route has a source and a
 *       destination, a length and a slope, which determine where the Vehicle
 *       will move and how.
 */

public class VenueRoute {

	/**
	 * The source location
	 */
    private int source;
    
    /**
     * the destination location
     */
    private int destination;
    
    /**
     * The length of the edge (weight)
     */
    private double length;
    
    /**
     * the slope of an edge or road
     */
    private double slope;

    /**
     * Static enum allows for 4 states for the Route.
     * @author Tyler
     *
     */
    public static enum RouteType {
        SMOOTH, DIRT, HILL, HAIRPIN
    };
    
    /**
     * The type of route
     */
    private RouteType rT;

    
    ////*** Constructor ***////
    
    public VenueRoute(int source, int destination, double length, double slope) {
        this.source = source;
        this.destination = destination;
        this.length = length;
        this.slope = slope;
        rT = RouteType.SMOOTH;

        // TODO: randomly choose route type.
        double chance = Math.random() * 10;
        if (slope != 0) {
            if (chance > 8) {
                rT = RouteType.HAIRPIN;
            } else if (chance > 6) {
                rT = RouteType.HILL;
            } else if (chance > 3) {
                rT = RouteType.DIRT;
            } else {
                rT = RouteType.SMOOTH;
            }
        } else {
            if (chance > 8) {
                rT = RouteType.HILL;
            } else if (chance > 5) {
                rT = RouteType.DIRT;
            } else {
                rT = RouteType.SMOOTH;
            }
        }
    }

    /*** Getters and Setters *****/
    
    /**
     * 
     * @return the source location
     */
    public int getSource() {
        return source;
    }
    
    /**
     * 
     * @return the destination location
     */
    public int getDestination() {
        return destination;
    }
    
    /**
     * 
     * @return the length of an edge
     */
    public double getLength() {
        return length;
    }
    
    /**
     *
     * @return the slope of a route
     */
    public double getSlope() {
        return slope;
    }
    
    /**
     * @return the routeType as specified by the enum
     */
    public RouteType getRouteType() {
        return rT;
    }

    /*** Used to change the enumerated type of the class ****/
    public void setRouteType(RouteType newRT) {
        rT = newRT;
    }

    /**
     * 
     * @param newDest
     * 					The new destination location
     */
    public void setDestination(int newDest) {
        destination = newDest;
    }

    /**
     * 
     * @param newSource
     * 					new source node
     */
    public void setSource(int newSource) {
        source = newSource;
    }
    
    /**
     * 
     * @param newLength
     * 					The new weight of an edge
     */
    public void setLength(double newLength) {
        length = newLength;
    }

    /**
     * 
     * @param newSlope
     * 					the new slope value
     */
    public void getSlope(double newSlope) {
        slope = newSlope;
    }

    /**
     * Returns a string representing this VenueRoute.
     * 
     * @author Chester Moses
     */
    public String toString() {
        return "route " + source + " -> " + destination + ", length: " + length + ", slope: " + slope;
    }

    /**
     * Equals method returns true if object is an instance of VenueRoute with
     * the same source and destination.
     */
    public boolean equals(Object other) {
        if (!((other instanceof VenueRoute))) {
            return false;
        }
        VenueRoute o = (VenueRoute) other;
        if (o.source == this.source && o.destination == this.destination) {
            return true;
        } else {
            return false;
        }
    }
}
